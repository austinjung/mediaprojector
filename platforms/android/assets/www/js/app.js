// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('mediaProjector', [
    'ionic',
    'ngCordova',
    'mediaProjector.controllers',
    'mediaProjector.services'
])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function($ionicPopup) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }

        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleLightContent();
        }

        if (window.ConnectSDK) {
            window.ConnectSDK.discoveryManager.startDiscovery();
            console.log("window.ConnectSDK.discoveryManager.startDiscovery started.");
        }

    });
})

.config(function($ionicConfigProvider) {
    if(!ionic.Platform.isAndroid())$ionicConfigProvider.scrolling.jsScrolling(false);
})

.config(function($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

    // setup an abstract state for the tabs directive
        .state('tab', {
        url: "/tab",
        abstract: true,
        templateUrl: "templates/tabs.html"
    })

    // Each tab has its own nav history stack:

    .state('tab.public-channels', {
        url: '/public-channels',
        views: {
            'tab-public-channels': {
                templateUrl: 'templates/tab-public-channels.html',
                controller: 'PublicChannelsCtrl'
            }
        }
    })
    .state('tab.channel-detail', {
        url: '/public-channels/:channelId',
        views: {
            'tab-public-channels': {
                templateUrl: 'templates/channel-detail.html',
                controller: 'ChannelDetailCtrl'
            }
        }
    })

    /*

    .state('tab.latest', {
        url: '/latest',
        views: {
            'tab-latest': {
                templateUrl: 'templates/tab-latest.html',
                controller: 'LatestCtrl'
            }
        }
    })

    .state('tab.filter', {
        url: '/filter',
        views: {
            'tab-filter': {
                templateUrl: 'templates/tab-filter.html',
                controller: 'FilterCtrl'
            }
        }
    })

    .state('tab.chats', {
        url: '/chats',
        views: {
            'tab-chats': {
                templateUrl: 'templates/tab-chats.html',
                controller: 'ChatsCtrl'
            }
        }
    })
    .state('tab.chat-detail', {
        url: '/chats/:chatId',
        views: {
            'tab-chats': {
                templateUrl: 'templates/chat-detail.html',
                controller: 'ChatDetailCtrl'
            }
        }
    })
    */

    .state('tab.account', {
        url: '/account',
        views: {
            'tab-account': {
                templateUrl: 'templates/tab-account.html',
                controller: 'AccountCtrl'
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/public-channels');

});
