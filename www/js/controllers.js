angular.module('mediaProjector.controllers', ['ionic'])

.controller('RootCtrl', function($scope, $http, $log, $ionicPopup, $cordovaNetwork, $cordovaGeolocation, $ionicSideMenuDelegate, $cordovaDialogs, $timeout) {

    $scope.toggleMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    }

    $scope.selectCategories = function(categories) {
        $log.debug('Select Categories');
        $scope.toggleMenu();
        $scope.$broadcast('selectCategories', categories);
    }

    $scope.filterByTitle = function() {
        $log.debug('Filter by title');
        $cordovaDialogs.prompt('미디아 제목에서 찾는 단어를 입력하세요.', 'Search', ['찾기','취소'], '')
            .then(function(result) {
                var input = result.input1;
                // no button = 0, 'OK' = 1, 'Cancel' = 2
                var btnIndex = result.buttonIndex;
                $log.debug('Filter by title : ' + input + ", " + btnIndex);
                $scope.toggleMenu();
                $scope.$broadcast('filterByTitle', input);
        });
    }

    $scope.refreshVideoList = function() {
        $log.debug('Broadcast refreshVideoList');
        $scope.$broadcast('refreshVideoList');
    }

    $scope.getGeolocation = function(position) {
        $scope.apiurl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.coords.latitude + "," + position.coords.longitude + "&sensor=true";
        $http.post($scope.apiurl)
            .success(function(data) {
                window.device.position.geolocation = true;
                $log.debug("Get Geolocation details.");
                var address_components = data.results[0].address_components;
                for (var i=0; i<address_components.length; i++) {
                    if (address_components[i].types[0] == "neighborhood") {
                        window.device.position.neighborhood = address_components[i].long_name;
                        $log.debug("neighborhood : " + window.device.position.neighborhood);
                    } else if (address_components[i].types[0] == "locality") {
                        window.device.position.locality = address_components[i].long_name;
                        $log.debug("locality : " + window.device.position.locality);
                    } else if (address_components[i].types[0] == "administrative_area_level_2") {
                        window.device.position.administrative_area_level_2 = address_components[i].long_name;
                        $log.debug("administrative_area_level_2 : " + window.device.position.administrative_area_level_2);
                    } else if (address_components[i].types[0] == "administrative_area_level_1") {
                        window.device.position.administrative_area_level_1 = address_components[i].long_name;
                        $log.debug("administrative_area_level_1 : " + window.device.position.administrative_area_level_1);
                    } else if (address_components[i].types[0] == "country") {
                        window.device.position.country = address_components[i].long_name;
                        $log.debug("country : " + window.device.position.country);
                    } else if (address_components[i].types[0] == "postal_code") {
                        window.device.position.postal_code = address_components[i].long_name;
                        $log.debug("postal_code : " + window.device.position.postal_code);
                    }
                }
                if (window.device && window.networkinterface && window.networkinterface.myIP) {
                    $log.debug("Device and IP information is ready to getData.");
                    $scope.refreshVideoList();
                }
            })
            .error(function(data, status) {
                if (status !== 0 && data !== null) {
                    $log.error("Could not retrieve Geolocation. Http error " + status + ".");
                    $log.debug(data);
                }
            });
    };

    document.addEventListener("deviceready", function () {
        var type = $cordovaNetwork.getNetwork()
        var isOnline = $cordovaNetwork.isOnline()
        var isOffline = $cordovaNetwork.isOffline()

        $log.debug('Device network status type : ' + type + ', Online : ' + isOnline + ', Offline : ' + isOffline);
        if (window.device) {
            $log.debug("cordovaDevice UUID : " + window.device.uuid);
            $log.debug("cordovaDevice Platform : " + window.device.platform);
            $log.debug("cordovaDevice Version : " + window.device.version);
            $log.debug("cordovaDevice Model : " + window.device.model);
            $log.debug("cordovaDevice Manufacturer : " + window.device.manufacturer);
        }

        var posOptions = {timeout: 10000, enableHighAccuracy: false};
        $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function (position) {
            window.device.position = position;
            $log.debug("Get Geolocation latitude:" + window.device.position.coords.latitude + ", longitude:" + window.device.position.coords.longitude);
            $scope.getGeolocation(position);
        }, function(err) {
            $log.debug("Can't get Geolocation.");
            if (window.device && window.networkinterface && window.networkinterface.myIP) {
                $log.debug("Device and IP information is ready to getData.");
                $scope.refreshVideoList();
            }
        });

        if($cordovaNetwork.isOnline() == true) {
            if (window.networkinterface) {
                $log.debug("window.networkinterface exist.");
                window.networkinterface.getIPAddress(
                    function (ip) {
                        window.networkinterface.myIP = ip;
                        $log.debug("window.networkinterface.getIPAddress done IP : " + window.networkinterface.myIP);
                    }
                );
            }
        }

        if(navigator.connection.type == Connection.NONE) {
            var network_alert = $ionicPopup.alert({
                title: "Internet Disconnected",
                content: "인터넷이 연결되어 있지 않습니다. 인터넷이 연결된후 사용하십시요"
            });
            network_alert.then(function(result) {
                ionic.Platform.exitApp();
            });
            $timeout(function() {
                network_alert.close(); //close the popup after 3 seconds for some reason
                ionic.Platform.exitApp();
            }, 5000);
        }

        // listen for Online event
        $scope.$on('$cordovaNetwork:online', function(event, networkState){
            $log.debug('Device Online event detected.');
            if (window.networkinterface) {
                $log.debug("window.networkinterface exist.");
                window.networkinterface.getIPAddress(
                    function (ip) {
                        window.networkinterface.myIP = ip;
                        $log.debug("window.networkinterface.getIPAddress done IP : " + window.networkinterface.myIP);
                        if (window.device && window.networkinterface) {
                            $log.debug("Device and IP information is ready to getData.");
                            $scope.refreshVideoList();
                        }
                    }
                );
            }
        })

        // listen for Offline event
        $scope.$on('$cordovaNetwork:offline', function(event, networkState){
            $log.debug('Device Offline event detected.');
            var network_alert = $ionicPopup.alert({
                title: "Internet Disconnected",
                content: "인터넷 연결이 끊어졌습니다. 인터넷이 연결된후 사용하십시요"
            });
            network_alert.then(function(result) {
                ionic.Platform.exitApp();
            });
            $timeout(function() {
                network_alert.close(); //close the popup after 3 seconds for some reason
                ionic.Platform.exitApp();
            }, 5000);
        })

    }, false);


})

.controller('PublicChannelsCtrl', function($scope, $ionicLoading, $log, $http, $cordovaToast) {
    $scope.init = function(apiurl) {
        $scope.apiurl = apiurl;
        $scope.data = null;
        $scope.publicChannels = null;
        $scope.promise = null;
        $scope.query = {
            ip: '',
            uuid: '',
            platform: '',
            version: '',
            model: '',
            manufacturer: '',
            title: '',
            categories: '',
            date: '',
            search: '',
            ordering: '',
            page_size: 20,
            page: 1
        };
        $scope.query_stats = {
            first_item: null,
            last_item: null,
            total_items: null
        };
        $log.debug('PublicChannelsCtrl Init');
    };

    $scope.$on('refreshVideoList', function(e) {
        if ($scope.apiurl) {
            $log.debug('LatestCtrl Catch refreshVideoList with myIP : ' + window.networkinterface.myIP);

            $scope.query.page = 1;
            $scope.query.ip = window.networkinterface.myIP;
            $scope.query.uuid = window.device.uuid;
            $scope.query.platform = window.device.platform;
            $scope.query.version = window.device.version;
            $scope.query.model = window.device.model;
            $scope.query.manufacturer = window.device.manufacturer;

            $scope.getData();
        }
    });

    $scope.$on('filterByTitle', function(event, title) {
        if ($scope.apiurl) {
            $log.debug('LatestCtrl Catch filterByTitle with : ' + title);

            $scope.query.page = 1;
            $scope.query.title = title;

            $scope.getData();
        }
    });

    $scope.$on('selectCategories', function(event, categories) {
        if ($scope.apiurl) {
            $log.debug('LatestCtrl Catch selectCategories with : ' + categories);

            $scope.query.page = 1;
            $scope.query.categories = categories;

            $scope.getData();
        }
    });

    $scope.updateQueryStats = function() {
        $scope.query_stats.first_item = ($scope.query.page - 1) * $scope.query.page_size + 1;
        $scope.query_stats.last_item = $scope.query_stats.first_item + $scope.data.results.length - 1;
        $scope.query_stats.total_items = $scope.data.count;
    };

    $scope.updateQueryStatsWhenGetMore = function() {
        $scope.query_stats.last_item += $scope.data.results.length;
        $scope.query_stats.total_items = $scope.data.count;
    };

    $scope.canGetMoreData = function() {
        if ($scope.query_stats.last_item && ($scope.query_stats.last_item == $scope.query_stats.total_items)) {
            return false;
        } else {
            return true;
        }
    }

    $scope.getMoreData = function() {
        if ($scope.canGetMoreData() == false) {
            return;
        }
        if ($scope.promise) {
            return; // Cancel current http request if there is a pending http request
        }
        $scope.query.page += 1;
        $log.debug('Page: ' + $scope.query.page);
        $ionicLoading.show({
            template: '<ion-spinner class="spinner-positive" icon="ios"></ion-spinner>'
        });
        $scope.promise = $http.get($scope.apiurl, {params: $scope.query})
            .success(function(data) {
                $scope.data = data;
                if ($scope.publicChannels==null) {
                    $scope.publicChannels = data.results;
                } else {
                    $scope.publicChannels = $scope.publicChannels.concat(data.results);
                }
                $scope.updateQueryStatsWhenGetMore();
            })
            .error(function(data, status) {
                $ionicLoading.hide();
                $scope.promise = null;
                if (status !== 0 && data !== null) {
                    var msg = "Could not retrieve video list. Http error " + status + ".";
                    $log.error(msg);
                    $log.debug(data);
                    $cordovaToast.show(msg, 'long', 'bottom')
                                 .then(function(success){$log.debug('toast success: ' + success)}, function(error){alert('toast error: ' + error)});
                }
            })
            .then(function() {
                $ionicLoading.hide();
                $scope.promise = null;
            });
    };

    $scope.getData = function() {
        if ($scope.promise) {
            return; // Cancel current http request if there is a pending http request
        }
        $ionicLoading.show({
            template: '<ion-spinner class="spinner-positive" icon="ios"></ion-spinner>'
        });
        $scope.promise = $http.get($scope.apiurl, {params: $scope.query})
            .success(function(data) {
                $scope.data = data;
                $scope.publicChannels = data.results;
                $scope.updateQueryStats();
            })
            .error(function(data, status) {
                $ionicLoading.hide();
                $scope.promise = null;
                if (status !== 0 && data !== null) {
                    var msg = "Could not retrieve video list. Http error " + status + ".";
                    $log.error(msg);
                    $log.debug(data);
                    $cordovaToast.show(msg, 'long', 'bottom')
                                 .then(function(success){$log.debug('toast success: ' + success)}, function(error){alert('toast error: ' + error)});
                }
            })
            .then(function() {
                $ionicLoading.hide();
                $scope.promise = null;
            });
    };
})

.controller('ChannelDetailCtrl', function($scope, $stateParams, $ionicLoading, $log, $http, $cordovaToast) {
    $scope.init = function(apiurl) {
        $scope.apiurl = apiurl + $stateParams.channelId + "/";
        $scope.channel = null;
        $scope.promise = null;
        $scope.query = {
            ip: window.networkinterface.myIP,
            uuid: window.device.uuid,
            platform: window.device.platform,
            version: window.device.version,
            model: window.device.model,
            manufacturer: window.device.manufacturer,
            neighborhood: '',
            locality: '',
            administrative_area_level_2: '',
            administrative_area_level_1: '',
            country: '',
            postal_code: '',
        };
        if (window.device.position) {
            if (window.device.position.neighborhood) {
                $scope.query.neighborhood = window.device.position.neighborhood;
            }
            if (window.device.position.locality) {
                $scope.query.locality = window.device.position.locality;
            }
            if (window.device.position.administrative_area_level_2) {
                $scope.query.administrative_area_level_2 = window.device.position.administrative_area_level_2;
            }
            if (window.device.position.administrative_area_level_1) {
                $scope.query.administrative_area_level_1 = window.device.position.administrative_area_level_1;
            }
            if (window.device.position.country) {
                $scope.query.country = window.device.position.country;
            }
            if (window.device.position.postal_code) {
                $scope.query.postal_code = window.device.position.postal_code;
            }
        }
        $log.debug('ChannelDetailCtrl Init : ' + $scope.apiurl);
        $scope.getData();
    };

    $scope.getData = function() {
        if ($scope.promise) {
            return; // Cancel current http request if there is a pending http request
        }
        $ionicLoading.show({
            template: '<ion-spinner class="spinner-positive" icon="ios"></ion-spinner>'
        });
        $log.debug("get Channel IP :" + $scope.query.ip);
        $scope.promise = $http.get($scope.apiurl, {params: $scope.query})
            .success(function(data) {
                $scope.channel = data;
                $log.debug("get MediaURL :" + $scope.channel.MediaURL);
            })
            .error(function(data, status) {
                $ionicLoading.hide();
                $scope.promise = null;
                if (status !== 0 && data !== null) {
                    var msg = "Could not retrieve video. Http error " + status + ".";
                    $log.error(msg);
                    $log.debug(data);
                    $cordovaToast.show(msg, 'long', 'bottom')
                                 .then(function(success){$log.debug('toast success: ' + success)}, function(error){alert('toast error: ' + error)});
                }
            })
            .then(function() {
                $ionicLoading.hide();
                $scope.promise = null;
            });
    };

    $scope.launch_video = function() {
        // Play a video with callbacks
        var options = {
            successCallback: function() {
                $log.debug("Video was closed without error.");
            },
            errorCallback: function(errMsg) {
                $cordovaToast.show('Error on streaming video! \nYou might not have any video streaming app.', 'long', 'bottom')
                             .then(function(success){$log.debug('toast success: ' + success)}, function(error){alert('toast error: ' + error)});
                $log.debug("Video Streaming Error! " + errMsg);
            }
        };
        $log.debug("launch_video in Channel:" + $scope.channel.MediaURL);
        window.plugins.streamingMedia.playVideo($scope.channel.MediaURL, options);
    };

    $scope.showDevicePicker = function () {
        if (window.ConnectSDK) {
            $log.debug("window.ConnectSDK.discoveryManager.pickDevice start.");
            window.ConnectSDK.discoveryManager.pickDevice().success(function (device) {
                var options = {
                    title: $scope.channel.WebsiteProgram.Program.Title,
                    iconUrl: $scope.channel.ThumbNail,
                    shouldLoop: true
                };
                function sendVideo () {
                    if ($scope.mediaControl) {
                        $scope.mediaControl.stop();
                        $scope.mediaControl = null;
                        $log.debug("Stop playing media " + $scope.mediaControl);
                    }
                    if ($scope.launchSession) {
                        $scope.launchSession.close();
                        $scope.launchSession = null;
                        $log.debug("Close new launchSession " + $scope.launchSession);
                    }
                    device.getMediaPlayer().playMedia($scope.channel.MediaURL_TV, $scope.channel.MediaMimeType, options)
                    .success(function (launchSession, mediaControl) {
                        $scope.launchSession = launchSession.acquire();
                        $scope.mediaControl = mediaControl && mediaControl.acquire();
                        $log.debug("Open new launchSession " + $scope.launchSession);
                    }).error(function (err) {
                        $log.debug("play video failure: " + err.message);
                    });
                }
                if (device.isReady()) { // already connected
                    sendVideo();
                } else {
                    device.on("ready", sendVideo);
                    device.connect();
                }
            })
            $log.debug("window.ConnectSDK.discoveryManager.pickDevice Done.");
        }
    };

})

.controller('LatestCtrl', function($scope, $ionicLoading, $log, $http, $cordovaToast) {
    $scope.init = function(apiurl) {
        $scope.apiurl = apiurl;
        $scope.data = null;
        $scope.latestVideos = null;
        $scope.launchSession = null;
        $scope.mediaControl = null;
        $scope.promise = null;
        $scope.query = {
            ip: '',
            uuid: '',
            platform: '',
            version: '',
            model: '',
            manufacturer: '',
            neighborhood: '',
            locality: '',
            administrative_area_level_2: '',
            administrative_area_level_1: '',
            country: '',
            postal_code: '',
            title: '',
            website: 'OnDemand',
            broadcaster: '',
            date: '',
            active: 'true',
            search: '',
            ordering: '',
            page_size: 20,
            page: 1
        };
        $scope.query_stats = {
            first_item: null,
            last_item: null,
            total_items: null
        };
        $log.debug('LatestCtrl Init');
    };

    $scope.$on('refreshVideoList', function(e) {
        if ($scope.apiurl) {
            $log.debug('LatestCtrl Catch refreshVideoList with myIP : ' + window.networkinterface.myIP);
            $scope.query.page = 1;
            $scope.query.ip = window.networkinterface.myIP;
            $scope.query.uuid = window.device.uuid;
            $scope.query.platform = window.device.platform;
            $scope.query.version = window.device.version;
            $scope.query.model = window.device.model;
            $scope.query.manufacturer = window.device.manufacturer;
            if (window.device.position) {
                if (window.device.position.neighborhood) {
                    $scope.query.neighborhood = window.device.position.neighborhood;
                } else {
                    $scope.query.neighborhood = null;
                }
                if (window.device.position.locality) {
                    $scope.query.locality = window.device.position.locality;
                } else {
                    $scope.query.locality = null;
                }
                if (window.device.position.administrative_area_level_2) {
                    $scope.query.administrative_area_level_2 = window.device.position.administrative_area_level_2;
                } else {
                    $scope.query.administrative_area_level_2 = null;
                }
                if (window.device.position.administrative_area_level_1) {
                    $scope.query.administrative_area_level_1 = window.device.position.administrative_area_level_1;
                } else {
                    $scope.query.administrative_area_level_1 = null;
                }
                if (window.device.position.country) {
                    $scope.query.country = window.device.position.country;
                } else {
                    $scope.query.country = null;
                }
                if (window.device.position.postal_code) {
                    $scope.query.postal_code = window.device.position.postal_code;
                } else {
                    $scope.query.postal_code = null;
                }
            }
            $scope.getData();
        }
    });

    $scope.updateQueryStats = function() {
        $scope.query_stats.first_item = ($scope.query.page - 1) * $scope.query.page_size + 1;
        $scope.query_stats.last_item = $scope.query_stats.first_item + $scope.data.results.length - 1;
        $scope.query_stats.total_items = $scope.data.count;
        $log.debug('LatestCtrl Query Stat first item : ' + $scope.query_stats.first_item);
        $log.debug('LatestCtrl Query Stat last item : ' + $scope.query_stats.last_item);
        $log.debug('LatestCtrl Query Stat total items : ' + $scope.query_stats.total_items);
    };

    $scope.updateQueryStatsWhenGetMore = function() {
        $scope.query_stats.last_item += $scope.data.results.length;
        $scope.query_stats.total_items = $scope.data.count;
        $log.debug('LatestCtrl WhenGetMore Query Stat first item : ' + $scope.query_stats.first_item);
        $log.debug('LatestCtrl WhenGetMore Query Stat last item : ' + $scope.query_stats.last_item);
        $log.debug('LatestCtrl WhenGetMore Query Stat total items : ' + $scope.query_stats.total_items);
    };

    $scope.canGetMoreData = function() {
        if ($scope.query_stats.last_item && ($scope.query_stats.last_item == $scope.query_stats.total_items)) {
            $log.debug('No more data');
            return false;
        } else {
            $log.debug('Can get more data');
            return true;
        }
    }

    $scope.getMoreData = function() {
        $log.debug('Get more data');
        if ($scope.canGetMoreData() == false) {
            $log.debug('Stop get more data because no more data');
            return;
        }
        if ($scope.promise) {
            $log.debug("Not ready to get more data");
            return; // Cancel current http request if there is a pending http request
        }
        $scope.query.page += 1;
        $log.debug('Page: ' + $scope.query.page);
        $ionicLoading.show({
            template: '<ion-spinner class="spinner-positive" icon="ios"></ion-spinner>'
        });
        $scope.promise = $http.get($scope.apiurl, {params: $scope.query})
            .success(function(data) {
                $log.debug("Receive new video");
                $scope.data = data;
                if ($scope.latestVideos==null) {
                    $scope.latestVideos = data.results;
                } else {
                    $scope.latestVideos = $scope.latestVideos.concat(data.results);
                }
                $scope.updateQueryStatsWhenGetMore();
            })
            .error(function(data, status) {
                $ionicLoading.hide();
                $scope.promise = null;
                if (status !== 0 && data !== null) {
                    var msg = "Could not retrieve video list. Http error " + status + ".";
                    $log.error(msg);
                    $log.debug(data);
                    $cordovaToast.show(msg, 'long', 'bottom')
                                 .then(function(success){$log.debug('toast success: ' + success)}, function(error){alert('toast error: ' + error)});
                }
            })
            .then(function() {
                $ionicLoading.hide();
                $log.debug("Ready to get more data");
                $scope.promise = null;
            });
    };

    $scope.getData = function() {
        $log.debug('Get Data');
        if ($scope.promise) {
            $log.debug("Not ready to get more data");
            return; // Cancel current http request if there is a pending http request
        }
        $log.debug('IP: ' + $scope.query.ip);
        $log.debug('UUID: ' + $scope.query.uuid);
        $log.debug('Platform: ' + $scope.query.platform);
        $log.debug('Version: ' + $scope.query.version);
        $log.debug('Model: ' + $scope.query.model);
        $log.debug('Manufacturer: ' + $scope.query.manufacturer);
        $log.debug('Page: ' + $scope.query.page);
        $ionicLoading.show({
            template: '<ion-spinner class="spinner-positive" icon="ios"></ion-spinner>'
        });
        $scope.promise = $http.get($scope.apiurl, {params: $scope.query})
            .success(function(data) {
                $log.debug("Receive first video");
                $scope.data = data;
                $scope.latestVideos = data.results;
                $scope.updateQueryStats();
            })
            .error(function(data, status) {
                $ionicLoading.hide();
                $scope.promise = null;
                if (status !== 0 && data !== null) {
                    var msg = "Could not retrieve video list. Http error " + status + ".";
                    $log.error(msg);
                    $log.debug(data);
                    $cordovaToast.show(msg, 'long', 'bottom')
                                 .then(function(success){$log.debug('toast success: ' + success)}, function(error){alert('toast error: ' + error)});
                }
            })
            .then(function() {
                $ionicLoading.hide();
                $log.debug("Ready to get more data");
                $scope.promise = null;
            });
    };

    $scope.launch_video = function(videoUrl) {
        // Play a video with callbacks
        var options = {
            successCallback: function() {
                $log.debug("Video was closed without error.");
            },
            errorCallback: function(errMsg) {
                $cordovaToast.show('Error on streaming video! \nYou might not have any video streaming app.', 'long', 'bottom')
                             .then(function(success){$log.debug('toast success: ' + success)}, function(error){alert('toast error: ' + error)});
                $log.debug("Video Streaming Error! " + errMsg);
            }
        };
        window.plugins.streamingMedia.playVideo(videoUrl, options);
    };

    $scope.showDevicePicker = function (videoUrl, videoTitle, videoThumbNail) {
        if (window.ConnectSDK) {
            $log.debug("window.ConnectSDK.discoveryManager.pickDevice start.");
            window.ConnectSDK.discoveryManager.pickDevice().success(function (device) {
                var options = {
                    title: videoTitle,
                    iconUrl: videoThumbNail,
                    shouldLoop: true
                };
                function sendVideo () {
                    if ($scope.mediaControl) {
                        $scope.mediaControl.stop();
                        $scope.mediaControl = null;
                        $log.debug("Stop playing media " + $scope.mediaControl);
                    }
                    if ($scope.launchSession) {
                        $scope.launchSession.close();
                        $scope.launchSession = null;
                        $log.debug("Close new launchSession " + $scope.launchSession);
                    }
                    device.getMediaPlayer().playMedia(videoUrl, "video/mp2t", options)
                    .success(function (launchSession, mediaControl) {
                        $scope.launchSession = launchSession.acquire();
                        $scope.mediaControl = mediaControl && mediaControl.acquire();
                        $log.debug("Open new launchSession " + $scope.launchSession);
                    }).error(function (err) {
                        $log.debug("play video failure: " + err.message);
                    });
                }
                if (device.isReady()) { // already connected
                    sendVideo();
                } else {
                    device.on("ready", sendVideo);
                    device.connect();
                }
            })
            $log.debug("window.ConnectSDK.discoveryManager.pickDevice Done.");
        }
    };

})

.controller('FilterCtrl', function($scope) {
    $scope.website = 'OnDemandKorea';
})

.controller('ChatsCtrl', function($scope, Chats) {
    $scope.chats = Chats.all();
    $scope.remove = function(chat) {
        Chats.remove(chat);
    }
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
    $scope.chat = Chats.get($stateParams.chatId);

    $scope.launch_video = function(videoUrl) {
        // Play a video with callbacks
        var options = {
            successCallback: function() {
                console.log("Video was closed without error.");
            },
            errorCallback: function(errMsg) {
                $cordovaToast.show('Error on streaming video! \nYou might not have any video streaming app.', 'long', 'bottom')
                             .then(function(success){console.log('toast success: ' + success)}, function(error){alert('toast error: ' + error)});
                console.log("Video Streaming Error! " + errMsg);
            }
        };
        window.plugins.streamingMedia.playVideo(videoUrl, options);
    };

    $scope.launch_audio = function(audioUrl, backgroundImageUrl) {
        // Play a video with options and callbacks
        var options = {
            bgColor: "#FFFFFF",
            bgImage: backgroundImageUrl,
            bgImageScale: "fit",
            successCallback: function() {
                console.log("Audio was closed without error.");
            },
            errorCallback: function(errMsg) {
                $cordovaToast.show('Error on streaming audio! \nYou might not have any audio streaming app.', 'long', 'bottom')
                             .then(function(success){console.log('toast success: ' + success)}, function(error){alert('toast error: ' + error)});
                console.log("Audio Streaming Error! " + errMsg);
            }
        };
        window.plugins.streamingMedia.playAudio(audioUrl, options);
    };

})

.controller('AccountCtrl', function($scope) {
    $scope.settings = {
        enableFriends: true
    };
});
